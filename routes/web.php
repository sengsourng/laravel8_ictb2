<?php

use App\Models\User;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TagController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\PermissionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('admin.dashboard.home');
    return view('welcome');
    // return view('auth.login');
    // return view('admin.dashboard.home2');
    // return view('layouts.admin_login');

});



// Route::resource('customers','CustomerController');
Route::resource('customers',CustomerController::class);
Route::resource('products',ProductController::class);


Route::get('student','App\Http\Controllers\StudentController@index');
Route::post('student','App\Http\Controllers\StudentController@store')->name('student.store');
Route::get('student/{id}/edit', 'App\Http\Controllers\StudentController@edit')->name('student.edit');
Route::post('student/update', 'App\Http\Controllers\StudentController@update')->name('student.update');
Route::get('student/{id}/delete', 'App\Http\Controllers\StudentController@destroy')->name('student.delete');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('/admin/users',UserController::class);


Route::resource('/admin/customers',CustomerController::class);
Route::resource('/admin/products',ProductController::class);

Route::resource('/admin/tags',TagController::class);
Route::resource('/admin/brands', BrandController::class);

// Route::post('/admin/check_brand/{brand_id}',BrandController::class);
Route::post('/admin/check_brand', array('as' => '', 'uses' => 'BrandController@check_brand'));


Route::get('/roles',[App\Http\Controllers\PermissionController::class,'Permission']);

// Route::get('/roles', 'PermissionController@Permission');

Route::group(['middleware' => 'role:developer'], function() {
    Route::get('/admin', function() {
       return 'Welcome Admin';
    });

 });

