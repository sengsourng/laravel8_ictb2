<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Brand;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;

class BrandController extends Controller
{
    public function __construct()
    {
    $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=[];
        $data['UserRegisters']=User::take(3)->get();
        $data['Brands']=Brand::get();


        return view('admin.brands.index',$data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[];
        $data['UserRegisters']=User::take(3)->get();
        $data['Brands']=Brand::get();

        return view('admin.brands.create',$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->slug!=''){
            $slug=$this->make_slug($request->slug);
        }else{
            $slug=$this->make_slug($request->name);
        }
        Brand::create([
            'name'=>$request->name,
            'slug'=>$slug,
            'image'=>'noimage.jpg',
            'note'=>$request->note,
            'status' => $this->Status($request),
            'user_id'=>Auth::user()->id,
            'created_at'=>$request->created_at,

         ]);



         Toastr::success('Brand created successfully :)', 'Successfully');
         return redirect()->route('brands.create')
            ->with('success','Brand created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=[];
        $data['UserRegisters']=User::take(3)->get();
        $data['Brands']=Brand::get();

        $data['BrandEdit']=Brand::findOrFail($id);

        return view('admin.brands.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand=Brand::findOrFail($id);
        if($request->slug!=''){
            $slug=$this->make_slug($request->slug);
        }else{
            $slug=$this->make_slug($request->name);
        }
        $brand->update([
            'name'=>$request->name,
            'slug'=> $slug,
            'status' => $this->Status($request),
            'user_id'=>Auth::user()->id,
            'updated_at'=>$request->updated_at,
        ]);

        Toastr::success('Brand Update successfully :)', 'Successfully');
         return redirect()->route('brands.index')
            ->with('success','Brand Update successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $Brand =Brand::where('id',$id)->firstOrFail();
        $Brand->delete();
        Toastr::success('Tag Delete successfully :)', 'Successfully');
        return redirect()->back();

    }
}
