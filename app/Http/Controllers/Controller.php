<?php

namespace App\Http\Controllers;

// use GuzzleHttp\Psr7\Request;
use App\Models\Brand;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


     // ប្រើសម្រាប់​បំប្លែង​ slug ទាំង​ភាសាខ្មែរ​នឹង​ English
     public function make_slug($string) {
        return preg_replace('/\s+/u', '-', trim($string));
    }

    public function Status(Request $request){
        if ($request->status) {
            $status = true;
        } else {
            $status = false;
        }
        return $status;
    }

    public function check_brand($brand_id){
        // if(Brand::where('first_name','=',$request->input('superior_fname'))
        //             ->where('last_name','=',$request->input('superior_lname'))
        //             ->exists()){
        //    return "exist";
        // }else{
        //    return "not exist";
        // }

        if(Brand::where('id','=',$brand_id)
                    // ->where('last_name','=',$request->input('superior_lname'))
                    ->exists()){
           return "exist";
        }else{
           return "not exist";
        }

    }

    public function check_product($product_id){
        // if(Brand::where('first_name','=',$request->input('superior_fname'))
        //             ->where('last_name','=',$request->input('superior_lname'))
        //             ->exists()){
        //    return "exist";
        // }else{
        //    return "not exist";
        // }

        if(Product::where('id','=',$product_id)
                    // ->where('last_name','=',$request->input('superior_lname'))
                    ->exists()){
           return "exist";
        }else{
           return "not exist";
        }

    }

}
