<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\User;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;

class TagController extends Controller
{
    public function index(){
        $data=[];
        $data['UserRegisters']=User::take(3)->get();
        $data['Tags']=Tag::get();

        return view('admin.tags.index',$data);
    }

    public function create(){
        $data=[];
        $data['UserRegisters']=User::take(3)->get();
        $data['Tags']=Tag::get();

        return view('admin.tags.create',$data);
    }
    public function store(Request $request){
        if($request->slug!=''){
            $slug=$this->make_slug($request->slug);
        }else{
            $slug=$this->make_slug($request->name);
        }
        Tag::create([
            'name'=>$request->name,
            'slug'=>$slug,
            'status' => $this->Status($request),
            'user_id'=>Auth::user()->id,
            'created_at'=>$request->created_at,

         ]);

         Toastr::success('Tag created successfully :)', 'Successfully');
         return redirect()->route('tags.create')
            ->with('success','Tag created successfully.');
    }

    public function destroy($id)
    {
        $Tag =Tag::where('id',$id)->firstOrFail();
        $Tag->delete();

        if($Tag==false){
            Toastr::success('Tag Delete successfully :)', 'Successfully');
        }


        return redirect()->route('tags.index')
            ->with('success', 'Tag deleted successfully');

    }

    public function edit($id)
    {
        $data=[];
        $data['UserRegisters']=User::take(3)->get();
        $data['Tags']=Tag::get();
        $data['TagEdit'] =Tag::where('id',$id)->firstOrFail();

        return view('admin.tags.edit',$data);

    }

    public function update(Request $request, $id)
    {
        $data=[];
        $Tag =Tag::where('id',$id)->firstOrFail();
        $Tag->update([
            'name'=>$request->name,
            'slug'=>$request->slug,
            'status' => $this->Status($request),
            'user_id'=>Auth::user()->id,
            'updated_at'=>$request->updated_at,

         ]);

         Toastr::success('Tag Update successfully :)', 'Successfully');
         return redirect()->route('tags.index')
            ->with('success', 'Tag Update successfully');

    }

}
