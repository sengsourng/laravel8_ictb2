<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\User;
use App\Models\Brand;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data=[];
        $data['isTable']=true;
        $data['UserRegisters']=User::take(3)->get();
        // dd($data);
        $data['Users']=User::get();
        $data['Products']=Product::get();
        // dd($data['Products']);

        return view('admin.products.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[];
        // $data['isTable']=false;
        $data['UserRegisters']=User::take(3)->get();

        return view('admin.products.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $imageName="noimage.jpg";
        $status=$request->status==1?true:false;
        $featured=$request->featured==1?true:false;
        $rules=[
            'brand_id'=> 'required|int',
            'sku'=>'required|string',
            'name'  =>  'required|string',
            'quantity'  => 'required|int',
            'product_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];


        if($request->hasFile('product_image')){
            $image = $request->file('product_image');
            $imageName = 'product'.'-'.rand().'-'.time().'.'.$image->getClientOriginalExtension(); // generate new name
            $image->move(public_path('upload/products/'), $imageName);
            $imageName='upload/products/'.$imageName;
        }else{
            $imageName='upload/products/'.$imageName;
        }

        if ($request->slug!=""){
            $slug=$request->slug;
        }else{
            $slug=$this->make_slug($request->name);
        }


         Product::create([
            'name'=>$request->name,
            'brand_id'=>$request->brand_id,
            'sku'=>$request->sku,
            'slug'=>$slug,
            'description'=>$request->description,
            'weight'=>$request->weight,
            'quantity'=>$request->quantity,
            'price'=>$request->price,
            'sale_price'=>$request->sale_price,
            'status'=>$status,
            'featured'=>$featured,
            'user_id' => Auth::user()->id,
            'updated_at'=>$request->updated_at,
            'product_image'=>'upload/products/'.$imageName
         ]);

         Toastr::success('Product created successfully :)', 'Successfully');

        return redirect()->route('products.index')
            ->with('success', 'Product created successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=[];
        $data['UserRegisters']=User::take(3)->get();
        $data['brands']=Brand::get();

        $data['product'] =Product::where('id',$id)->firstOrFail();

        return view('admin.products.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $product =Product::where('id',$id)->firstOrFail();
        $old_file=$product->product_image;

        $imageName=null;
         if($request->hasFile('product_image')){
            //  dd($old_file);
             @unlink($old_file);
            $image = $request->file('product_image');
            $imageName = 'product'.'-'.rand().'-'.time().'.'.$image->getClientOriginalExtension(); // generate new name
            $image->move(public_path('upload/products/'), $imageName);
            $imageName='upload/products/'.$imageName;
        }else{
            $imageName=$old_file;
        }

        $status=$request->status==1?true:false;
        $featured=$request->featured==1?true:false;

        if ($request->slug!=""){
            $slug=$request->slug;
        }else{
            $slug=$this->make_slug($request->name);
        }
        //dd($request->featured);
         $product->update([
            'name'=>$request->name,
            'brand_id'=>$request->brand_id,
            'sku'=>$request->sku,
            'slug'=>$slug,
            'description'=>$request->description,
            'weight'=>$request->weight,
            'quantity'=>$request->quantity,
            'price'=>$request->price,
            'sale_price'=>$request->sale_price,
            'status'=>$status,
            'featured'=>$featured,
            'user_id' => Auth::user()->id,
            'created_at'=>$request->created_at,
            'product_image'=>$imageName
         ]);

         Toastr::success('Update successfully :)', 'Successfully');

        return redirect()->route('products.index')
            ->with('success', 'Product created successfully.');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Product =Product::where('id',$id)->firstOrFail();
        // return $Product->brand_id;
        $brand_id=$Product->brand_id;
        $checkBrand=$this->check_brand($brand_id);
       //    return $this->check_brand($Product->brand_id);
            if($checkBrand=='exist'){
                $Product->delete();
                Toastr::success('Product Exist :)', 'Exist');
                return redirect()->route('products.index')
                    ->with('success', 'Product has been use');

            }else{
                $Product->delete();
                Toastr::success('Product Delete successfully :)', 'Successfully');
                return redirect()->route('products.index')
                    ->with('success', 'Product deleted successfully');
            }
        }


}
