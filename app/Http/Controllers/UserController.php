<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Mobile;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
    $this->middleware('auth');
    }

    public function index(){
        //

        $data=[];
        // $data['isTable']=true;
        $data['UserRegisters']=User::take(3)->get();
        $data['Users']=User::get();
        // dd($data);

        return view('admin.users.index',$data);
    }
    public function create()
    {

        $data=[];
        $data['isTable']=false;
        $data['UserRegisters']=User::take(3)->get();

        return view('admin.users.create',$data);
    }

    public function store(Request $request)
    {

        // if ($request->user()->can('store-users')) {
            $request->validate([
                'name' => 'required',
                'email' => 'required',
                'profile_photo_path' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);


            if($request->hasFile('profile_photo_path')){
                $image = $request->file('profile_photo_path');
                $imageName = 'user'.'-'.rand().'-'.time().'.'.$image->getClientOriginalExtension(); // generate new name
                $image->move(public_path('upload/users/'), $imageName);

            }
             User::create([
                'name'=>$request->name,
                'email'=>$request->email,
                'password'=>Hash::make($request->password),
                // 'bio'=>$request->bio,
                // 'facebook'=>$request->facebook,
                // 'twiter'=>$request->twiter,
                // 'telegram'=>$request->telegram,
                'created_at'=>$request->created_at,
                'profile_photo_path'=>'upload/users/'.$imageName
             ]);

             Toastr::success('User created successfully :)', 'Successfully');
            //  Toastr::success('Post added successfully :)','Success');

            return redirect()->route('users.index')
                ->with('success', 'User created successfully.');

        // }


    }

    public function addUserMobile()
    {
        $user = new User;
        $user->name = "Test Name";
        $user->email = "test@mnp.com";
        $user->password = Hash::make("12345678");
        $user->save();

        $mobile = new Mobile;
        $mobile->mobile = '123456789';
        $user->mobile()->save($mobile);
    }

    public function show($id)
    {

        // $data=[];
        // $data['isTable']=false;
        // $data['UserRegisters']=User::take(3)->get();


        $data=[];
        $data['isTable']=false;
        $data['UserRegisters']=User::take(3)->get();

        $data['user'] =User::where('id',$id)->firstOrFail();


        return view('admin.users.show',$data);
    }
    public function edit($id)
    {
        $data=[];
        $data['isTable']=false;
        $data['UserRegisters']=User::take(3)->get();

        $data['user'] =User::where('id',$id)->firstOrFail();

        return view('admin.users.edit',$data);

    }


    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $data = [
            'name' => $request->name,
            // 'slug' => $this->make_slug($request->title)."_".rand(),
            'email' => $request->email,
            // 'category_id' => $request->category_id,
            // 'password' => $request->seo,
            // 'meta' => $request->meta,
            // 'status' => $this->Status($request),
            'profile_photo_path' => 'upload/users/'.$this->UpdateImage($request,$id),
            // 'user_id' => Auth::user()->id
        ];
        $user->update($data);
        // $post->tags()->sync($request->tag);
        return redirect()->route('users.index')->with('success', 'You are update post successfully!!!');
    }



    public function destroy($id)
    {

        $User =User::where('id',$id)->firstOrFail();
        $User->delete();

        Toastr::success('User Delete successfully :)', 'Successfully');
        return redirect()->route('users.index')
            ->with('success', 'User deleted successfully');
    }





    protected function uploadImage(Request $request)
    {
        /*ការ​បញ្ចូល​រូប​ភាព​បាន​មួយ​ file */
        if ($request->hasFile('profile_photo_path')) {
            $image = $request->file('profile_photo_path');
            $image_name = rand() . '_user' . '.' . $image->getClientOriginalExtension(); // generate new name
            // $image_name = $image->getClientOriginalName(); // get the original name of image
            $uploadpath = public_path('upload/users/');
            if (!is_dir($uploadpath)) {
                mkdir($uploadpath, 0777, true);
            }
            $image->move($uploadpath, $image_name);
        } else {
            $image_name = null;
        }
        return $image_name;
    }
    protected function UpdateImage(Request $request, $id)
    {
        if ($request->hasFile('profile_photo_path')) {
            $image_path = public_path('upload/users/' . $this->OldImage($id));
            $image_path = public_path($this->OldImage($id));
			if (file_exists($image_path)) {
                // dd($image_path);
				unlink($image_path);
			}
			$image = $request->file('profile_photo_path');
			$image_name = 'user' . '_' . rand() . '_' . '.' . $image->getClientOriginalExtension(); // generate new name
			// $image_name = $image->getClientOriginalName(); // get the original name of image
			$uploadpath = public_path('upload/users/');
			if (!is_dir($uploadpath)) {
				mkdir($uploadpath, 0777, true);
			}
			$image->move($uploadpath, $image_name);
		} else {
			$image_name = $this->OldImage($id);
        }
        return $image_name;
    }
    protected function OldImage($id)
    {
        $old_image = User::findOrFail($id)->profile_photo_path;
        return $old_image;
    }



}
