<?php

namespace App\Models;

use Psermission\HasPermissionsTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;


use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    use HasFactory, Notifiable;
    // use HasPermissionsTrait; //Import The Trait
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "users";

    protected $fillable = [
        'name',
        'email',
        'password',
        'profile_photo_path',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



    /**
     * Get the mobile number associated with the user.
     */
    public function mobile()
    {
        return $this->hasOne(Mobile::class);
        // note: we can also inlcude Mobile model like: 'App\Mobile'
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

}
