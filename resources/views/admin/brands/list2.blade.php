			<!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#ID</th>
                    <th>Name</th>
                    <th>Slug</th>
                    <th>Created at</th>

                  </tr>
                  </thead>
                  <tbody>

                    @foreach ($Brands as $item)
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>
                            @if ($item->status ==1)
                                <i class="fas fa-circle text-success"></i>
                            @else
                                <i class="fas fa-circle text-danger"></i>
                            @endif {{$item->name}}
                        </td>
                        <td>{{$item->slug}}</td>
                        <td>
                            <?php \Carbon\Carbon::setLocale('km') ?>
                            {{ \Carbon\Carbon::parse($item->created_at)->diffForHumans()}}

                        </td>

                      </tr>
                    @endforeach

                  </tbody>
                  <tfoot>
                    <tr>
                        <th>#ID</th>
                        <th>Name</th>
                        <th>slug</th>
                        <th>created at</th>

                      </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->

