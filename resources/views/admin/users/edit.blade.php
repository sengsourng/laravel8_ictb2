@extends('layouts.admin')

@push('css')

    <link rel="stylesheet" href="{{asset('admin/plugins/summernote/summernote-bs4.css')}}">
    @endpush

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add User</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
              <li class="breadcrumb-item active">Add user</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">


<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Edit User Information</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <section class="content" style="padding:15px;">
        {{-- @if(session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif --}}

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


        <form action="{{ route('users.update',$user->id) }}" method="POST" enctype="multipart/form-data">
            @csrf

            @method('PUT')

            <div class="row">
                <div class="col-md-8">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="UserName">Full Name</label>
                            <input type="text" id="UserName" name="name" value="{{$user->name}}" placeholder="SENG Sourng" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" id="email" name="email" value="{{$user->email}}" placeholder="sourng@gmail.com" class="form-control">
                        </div>

                        {{-- <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" id="password" value="{{$user->password}}" placeholder="12345678" name="password" class="form-control">
                        </div> --}}

                        <div class="form-group">
                            <label for="Bio">Bio</label>
                            <textarea id="Bio" class="form-control" value="{{$user->bio}}" name="bio" rows="4"></textarea>
                        </div>

                    </div>
                    <!-- /.card -->
                </div>
                <div class="col-md-4">


                    <div class="form-group">
                        <strong>Profile Image:</strong>
                        <br>
                        <img onclick="javascript:changeProfile()" id="preview" class="preview" src="{{$user->profile_photo_path==NULL?asset('upload/users/noimage.jpg'):asset($user->profile_photo_path)}}" width="100%" height="auto"/><br/>
                        <input type="file" name="profile_photo_path" id="image" class="image" style="display: none;"/>
                        {{-- <a href="javascript:changeProfile()" class="btn btn-success btn-xs imgupload" id="imgupload">Upload</a> | --}}
                        <a style="color: white; display:none;" href="javascript:removeImage()" class="btn btn-danger btn-xs remove" id="remove">Remove</a>

                    </div>



                    <div class="form-group">
                        <label for="roll">Roll</label>
                        <select class="form-control custom-select" id="roll" name="roll_id">
                            <option selected="" disabled="">Select one</option>
                            <option value="1">Admin</option>
                            <option value="2">Auther</option>
                        </select>
                    </div>

                </div>
            </div>
            <div class="card-footer">

                <button type="submit" class="btn btn-primary float-right ml-1">Save</button>
                {{-- <button type="submit" class="btn btn-danger float-right">Cancel</button> --}}
                <a href="{{route('users.index')}}" class="btn btn-danger float-right">Cancel</a>
            </div>
        </form>
    </section>

</div>

@endsection
@push('js')

<!-- Summernote -->
<script src="{{asset('admin/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>


<script>
    function changeProfile() {
      $('#image').click();
    }
    $('#image').change(function () {
      var imgPath = this.value;
      var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
      if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg") {
        $('#btn-remove').css('display','block');
        $('#btn-upload').text('Change');
        readURL(this);
      } else {
        alert("Please select image file (jpg, jpeg, png).");
      }
    });
    function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.readAsDataURL(input.files[0]);
          reader.onload = function (e) {

            $('#preview').attr('src', e.target.result);
            $('#remove').css('display','block');
          };

      }
    }
    function removeImage() {
      $('#preview').attr('src',"{{ asset('images/noimage.jpg') }}");
      $('#remove').css('display','none');

    }
  </script>



@endpush
