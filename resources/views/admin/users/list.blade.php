			<!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                      <th>#ID</th>
                      <th>Picture</th>
                      <th>Name</th>
                    <th>Qty</th>
                    <th>Price</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>

                    @foreach ($Users as $users)
                    <tr>
                        <td>{{$users->id}}</td>
                        <td><img class="img-size-50" src="{{asset($users->profile_photo_path)}}" alt=""></td>
                        <td>
                            @if ($users->status ==1)
                                <i class="fas fa-circle text-success"></i>
                            @else
                                <i class="fas fa-circle text-danger"></i>
                            @endif {{$users->name}}
                        </td>
                        <td>dd</td>
                        <td>dd</td>
                        <td>
                            <a class="btn btn-danger btn-sm" href="javascript:deleteObject({{ $users->id }})">Delete</a>
                            <a class="btn btn-warning btn-sm" href="{{route('users.edit',$users->id)}}">Edit</a>
                            <form id="frmItemDelete-{{ $users->id }}" style="display: none" action="{{ route('users.destroy',$users->id) }}" role="form" method="POST" enctype="multipart/form-data">
                                {{-- {{ csrf_field() }}
                                {{ method_field('DELETE') }} --}}
                                @method('DELETE')
                                @csrf

                            </form>
                        </td>
                      </tr>
                    @endforeach

                  </tbody>
                  <tfoot>
                    <tr>
                        <th>#ID</th>
                        <th>Picture</th>
                        <th>Name</th>
                        <th>Qty</th>
                        <th>Price</th>
                        <th>Action</th>
                      </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->

