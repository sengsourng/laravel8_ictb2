  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{asset('admin/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">SAToda ADMIN</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

            <li class="nav-item">
                <a href="{{url('/home')}}" class="nav-link {{ Request::is('home')?'active':'' }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>


                    {{-- <i class="right fas fa-angle-left"></i> --}}
                    <p>
                    Dashboard
                    <span class="right badge badge-danger">New</span>
                    </p>
                </a>
            </li>

            {{-- <li class="nav-item has-treeview menu-open"> --}}
          <li class="nav-item has-treeview {{ Request::is('admin/products*')?'menu-open':'' }} {{ Request::is('admin/brands*')?'menu-open':'' }} {{ Request::is('admin/tags*')?'menu-open':'' }}">
            <a href="{{url('admin/products')}}" class="nav-link {{ Request::is('admin/products')?'active':'' }}{{ Request::is('admin/products/create')?'active':'' }} {{ Request::is('admin/tags')?'active':'' }} ">
                <i class="nav-icon fas fa-list"></i>
              <p>
                Manage Products
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview ">
                <li class="nav-item">
                    <a href="{{url('admin/tags')}}" class="nav-link {{ Request::is('admin/tags')?'active':'' }}{{ Request::is('admin/tags/create')?'active':'' }} ">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Tag List</p>
                    </a>
                </li>
              <li class="nav-item">
                <a href="{{url('admin/brands')}}" class="nav-link {{ Request::is('admin/brands')?'active':'' }}{{ Request::is('admin/brands/create')?'active':'' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Brands</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('admin/products')}}" class="nav-link {{ Request::is('admin/products')?'active':'' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Product List</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('admin/products/create')}}" class="nav-link {{ Request::is('admin/products/create')?'active':'' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add Product</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index3.html" class="nav-link">
                  {{-- <i class="far fa-circle nav-icon"></i> --}}
                  <i class="fas fa-barcode"></i>
                  <p>Print Barcod</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index3.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Stock Count</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-shopping-cart"></i>
              <p>
                Manage Sales
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="./index.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sale List</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index2.html" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add Sale</p>
                </a>
              </li>
            </ul>
          </li>



          <li class="nav-item has-treeview {{ Request::is('admin/users*')?'menu-open':'' }}">
            <a href="{{url('admin/users')}}" class="nav-link {{ Request::is('admin/users')?'active':'' }}{{ Request::is('admin/users/create')?'active':'' }}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Manage People
                <i class="fas fa-angle-left right"></i>
                <span class="badge badge-info right">6</span>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('admin/users')}}" class="nav-link {{ Request::is('admin/users')?'active':'' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>User List</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('admin/users/create')}}" class="nav-link {{ Request::is('admin/users/create')?'active':'' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add User</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/layout/boxed.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Customer List</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/layout/fixed-sidebar.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add Customer</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/layout/fixed-topnav.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Supplier List</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/layout/fixed-footer.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add Supplier</p>
                </a>
              </li>

            </ul>


         </li>


         <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book-reader"></i>
              <p>
                Manage Blogs
                <i class="fas fa-angle-left right"></i>
                <span class="badge badge-info right">6</span>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/layout/top-nav.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Categories</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/layout/top-nav-sidebar.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Articles</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/layout/boxed.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Tags</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/layout/fixed-sidebar.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Menus</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/layout/fixed-topnav.html" class="nav-link">
                  {{-- <i class="far fa-circle nav-icon"></i> --}}
                  <i class="fas fa-bullhorn nav-icon"></i>
                  <p>Ads</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/layout/fixed-footer.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Slider</p>
                </a>
              </li>

            </ul>


         </li>


        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
