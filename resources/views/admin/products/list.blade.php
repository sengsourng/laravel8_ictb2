			<!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                      <th>#ID</th>
                      <th>Picture</th>
                      <th>Name</th>
                    <th>Qty</th>
                    <th>Price</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>

                    @foreach ($Products as $product)
                    <tr>
                        <td>{{$product->id}}</td>
                        <td><img class="img-size-50" src="{{asset($product->product_image)}}" alt=""></td>
                        <td>
                            @if ($product->status ==1)
                                <i class="fas fa-circle text-success"></i>
                            @else
                                <i class="fas fa-circle text-danger"></i>
                            @endif {{$product->name}}
                            <br>
                            <span style="color: blue;">{{$product->user->name}}</span> | <span style="color: blue;">{{$product->brand->name}}</span>
                        </td>
                        <td>{{$product->quantity}}</td>
                        <td>{{$product->price}}</td>
                        <td>
                            <a class="btn btn-danger btn-sm" href="javascript:deleteObject({{ $product->id }})">Delete</a>
                            <a class="btn btn-warning btn-sm" href="{{route('products.edit',$product->id)}}">Edit</a>
                            <form id="frmItemDelete-{{ $product->id }}" style="display: none" action="{{ route('products.destroy',$product->id) }}" role="form" method="POST" enctype="multipart/form-data">
                                {{-- {{ csrf_field() }}
                                {{ method_field('DELETE') }} --}}
                                @method('DELETE')
                                @csrf

                            </form>
                        </td>
                      </tr>
                    @endforeach

                  </tbody>
                  <tfoot>
                    <tr>
                        <th>#ID</th>
                        <th>Picture</th>
                        <th>Name</th>
                        <th>Qty</th>
                        <th>Price</th>
                        <th>Action</th>
                      </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->

