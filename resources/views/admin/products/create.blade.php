@extends('layouts.admin')
@section('title','Product Add')

@push('css')

<!-- DataTables -->
<link rel="stylesheet" href="{{asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@endpush
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Product</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
              <li class="breadcrumb-item active">Add Product</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">


<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Product Information</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <section class="content" style="padding:15px;">
        @if(session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <form action="{{route('products.store')}}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="row">
                <div class="col-md-8">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="ProductName">Product Name</label>
                            <input type="text" id="ProductName" name="name" placeholder="Name of your product" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="sku">SKU</label>
                            <input type="text" id="sku" name="sku" placeholder="P0001" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="Slug">Slug</label>
                            <input type="text" id="Slug" name="slug" placeholder="name-of-your-product" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea id="description" class="form-control" name="description" rows="4"></textarea>
                        </div>
                        {{-- quantity --}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="quantity">Qty</label>
                                    <input type="number" id="quantity" name="quantity" placeholder="10" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="weight">Weight</label>
                                    <input type="number" id="weight" name="weight" placeholder="10" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="price">Price</label>
                                    <input type="number" id="price" name="price" placeholder="10" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="sell_price">Sell Price</label>
                                    <input type="number" id="sell_price" name="sale_price" placeholder="12" class="form-control" required>
                                </div>
                            </div>
                        </div>




                    </div>
                    <!-- /.card -->
                </div>
                <div class="col-md-4">


                    <div class="form-group">
                        <strong>Product Image:</strong>
                        <br>
                        <img onclick="javascript:changeProfile()" id="preview" class="preview" src="{{ asset('images/noimage.jpg') }}" width="100%" height="auto"/><br/>
                        <input type="file" name="product_image" id="image" class="image" style="display: none;"/>
                        {{-- <a href="javascript:changeProfile()" class="btn btn-success btn-xs imgupload" id="imgupload">Upload</a> | --}}
                        <a style="color: white; display:none;" href="javascript:removeImage()" class="btn btn-danger btn-xs remove" id="remove">Remove</a>

                    </div>

                    <div class="form-group">
                        <label for="brand">Brand</label>
                        <select class="form-control custom-select" id="brand" name="brand_id" required>
                            <option selected="" disabled="">Select Brand</option>
                            <option value="1">ថ្នាំពេទ្យ​</option>
                            <option value="2">សំភារៈប្រើប្រាស់​</option>
                        </select>
                    </div>


                    <div class="form-group">
                        <label for="feature">Feature</label>
                        <select class="form-control custom-select" id="feature" name="featured" required>
                            <option selected="" disabled="">Select one</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select class="form-control custom-select" id="status" name="status" required>
                            <option selected="" disabled="">Select one</option>
                            <option value="1">Active</option>
                            <option value="0">Not Active</option>
                        </select>
                    </div>

                </div>
            </div>
            <div class="card-footer">

                <button type="submit" class="btn btn-primary float-right ml-1">Save</button>
                {{-- <button type="submit" class="btn btn-danger float-right">Cancel</button> --}}
                <a href="{{route('products.index')}}" class="btn btn-danger float-right">Cancel</a>
            </div>
        </form>
    </section>

</div>

@endsection
@push('js')

<!-- jQuery -->
<script src="{{('admin/plugins/jquery/jquery.min.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('admin/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>


<script>
    function changeProfile() {
      $('#image').click();
    }
    $('#image').change(function () {
      var imgPath = this.value;
      var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
      if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg") {
        $('#btn-remove').css('display','block');
        $('#btn-upload').text('Change');
        readURL(this);
      } else {
        alert("Please select image file (jpg, jpeg, png).");
      }
    });
    function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.readAsDataURL(input.files[0]);
          reader.onload = function (e) {

            $('#preview').attr('src', e.target.result);
            $('#remove').css('display','block');
          };

      }
    }
    function removeImage() {
      $('#preview').attr('src',"{{ asset('images/noimage.jpg') }}");
      $('#remove').css('display','none');

    }
  </script>
@endpush
