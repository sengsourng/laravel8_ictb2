@extends('layouts.admin')
@section('title','Tag List')

@push('css')

<!-- DataTables -->
<link rel="stylesheet" href="{{asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endpush
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Tag</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
              <li class="breadcrumb-item active">Add Tag</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">


<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Tag Information</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <section class="content" style="padding:15px;">
        @if(session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <form action="{{route('tags.update',$TagEdit->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('put')

            <div class="row">
                <div class="col-md-8">
                    @include('admin.tags.list2')

                </div>
                <div class="col-md-4">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="TagName">Tag Name</label>
                            <input type="text" id="TagName" name="name" placeholder="Enter Tag Name" class="form-control" value="{{$TagEdit->name}}" required>
                        </div>
                        <div class="form-group">
                            <label for="Slug">Slug</label>
                            <input type="text" id="Slug" name="slug" placeholder="name-of-your-tag" class="form-control" value="{{$TagEdit->name}}">
                        </div>

                        <div class="form-group">
                            <label for="status">Status</label>
                            <select class="form-control custom-select" id="status" name="status" required>
                                <option selected="" disabled="">Select one</option>
                                <option {{$TagEdit->status==1?'selected':''}} value="1">Active</option>
                                <option {{$TagEdit->status==0?'selected':''}} value="0">Not Active</option>
                            </select>
                        </div>

                    </div>
                    <!-- /.card -->
                </div>

            </div>
            <div class="card-footer">

                <button type="submit" class="btn btn-primary float-right ml-1">Update</button>
                {{-- <button type="submit" class="btn btn-danger float-right">Cancel</button> --}}
                <a href="{{route('tags.index')}}" class="btn btn-danger float-right">Cancel</a>
            </div>
        </form>
    </section>

</div>

@endsection
@push('js')
<!-- page script -->
<script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true,
        "autoWidth": false,
      });
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>


<!-- DataTables -->
<script src="{{asset('admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<!-- Summernote -->
<script src="{{asset('admin/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>



@endpush
