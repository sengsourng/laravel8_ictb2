			<!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#ID</th>
                    <th>Name</th>
                    <th>Slug</th>
                    <th>Created at</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>

                    @foreach ($Tags as $item)
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>
                            @if ($item->status ==1)
                                <i class="fas fa-circle text-success"></i>
                            @else
                                <i class="fas fa-circle text-danger"></i>
                            @endif {{$item->name}}
                        </td>
                        <td>{{$item->slug}}</td>
                        <td>
                            {{-- {{$item->created_at}} --}}
                            <?php \Carbon\Carbon::setLocale('km') ?>
                            {{ \Carbon\Carbon::parse($item->created_at)->diffForHumans()}}

                        </td>
                        <td>
                            <a class="btn btn-danger btn-sm" href="javascript:deleteObject({{ $item->id }})">Delete</a>

                            <form id="frmItemDelete-{{ $item->id }}" style="display: none" action="{{ route('tags.destroy',$item->id) }}" role="form" method="POST" enctype="multipart/form-data">

                                @method('DELETE')
                                @csrf

                            </form>

                            <a class="btn btn-success btn-sm" href="{{route('tags.edit', $item->id)}}">Edit</a>
                        </td>
                      </tr>
                    @endforeach

                  </tbody>
                  <tfoot>
                    <tr>
                        <th>#ID</th>
                        <th>Name</th>
                        <th>slug</th>
                        <th>created at</th>
                        <th>Action</th>
                      </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->

